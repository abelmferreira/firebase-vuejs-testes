const admin = require('../config/firebase.js')
const response = require('../handlers/httpResponse.js')
const FieldValue = require('firebase-admin').firestore.FieldValue

const profileUpdate = (req, res) => {
  //console.log(req.headers)
  //console.log(req.body)

  if (req.method !== 'POST' && req.method !== 'OPTIONS') {
    return response.error(res, 'method_check', `Method denied ${req.method}`, 403)
  }
  
  if (!req.body.uid) {
    return response.error(res, 'uid_check', `UID missing ${JSON.stringify(req.body)}`, 400)
  }

  const data = {
    displayName: (`${req.body.name} ${req.body.lastname}`)
  }
  
  admin.auth().updateUser(req.body.uid, data)
    .then(updated => {
      console.info(`Auth user updated -> ${req.body.uid}`)
      admin.firestore().collection('users').doc(req.body.uid).update({
          name: req.body.name,
          lastname: req.body.lastname,
          updated_at: FieldValue.serverTimestamp()
        })
        .then(() => {
          console.log(`DB profile user updated -> ${req.body.uid}`)
          return response.success(res, updated)

        })
        .catch(e => {return response.error(res, 'db_update', e)})
    })
    .catch(e => {return response.error(res, 'auth_update', e)})
}


module.exports = (app) => {
  app.post('/profileUpdate', profileUpdate)

  return app
}

