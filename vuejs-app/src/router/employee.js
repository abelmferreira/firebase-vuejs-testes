import ListEmployee from '@/components/Employee/ListEmployee'
import NewEmployee from '@/components/Employee/NewEmployee'
import EditEmployee from '@/components/Employee/EditEmployee'
import ViewEmployee from '@/components/Employee/ViewEmployee'

export default [
    {
      path: '/employee',
      name: 'list-employee',
      component: ListEmployee
    },
    {
      path: '/employee/new',
      name: 'new-employee',
      component: NewEmployee,
    },
    {
      path: '/employee/edit/:employee_id',
      name: 'edit-employee',
      component: EditEmployee
    },
    {
      path: '/employee/:employee_id',
      name: 'view-employee',
      component: ViewEmployee
    }
  ]
