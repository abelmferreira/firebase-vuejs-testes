import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyC1PBdrm079K_YXp9un8bFETghfys32dDA",
  authDomain: "ytube-vuejs-firestore-tutorial.firebaseapp.com",
  databaseURL: "https://ytube-vuejs-firestore-tutorial.firebaseio.com",
  projectId: "ytube-vuejs-firestore-tutorial",
  storageBucket: "ytube-vuejs-firestore-tutorial.appspot.com",
  messagingSenderId: "397646886890"
}

const firebaseApp = firebase.initializeApp(firebaseConfig)

// Default export
// Firebase Global
export default firebaseApp

// Firestore
import 'firebase/firestore'

// Podemos usar import { firestore as db } from '@/config/firebase'
export function firestore() {
  return firebaseApp.firestore()
}

// ou import { db } from '@/config/firebase' com o export abaixo
export { firestore as db }

