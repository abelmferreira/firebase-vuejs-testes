import Vue from 'vue'
import Router from 'vue-router'

// import {store} from '../store'

import globals from './globals'
import User from './user'
import Employee from './employee'

Vue.use(Router)

const router = new Router({
  routes: [
    ...globals,
    ...Employee,
    ...User
  ],
  mode: 'history'
})

// router.beforeEach((to, from, next) => {
//   let currentUser = store.state.user
//   let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

//   if (requiresAuth && !currentUser) next('/user/signin')
//   else next()
// })


export default router
