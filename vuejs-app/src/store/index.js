import Vue from 'vue'
import Vuex from 'vuex'
import firebase from '@/config/firebase'
import { db } from '@/config/firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loadedMeetups: [
      {
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/New_york_times_square-terabass.jpg',
        id: 'afajfjadfaadfa323',
        title: 'Meetup in New York',
        date: new Date(),
        location: 'New York',
        description: 'New York, New York!'
      },
      {
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/7/7a/Paris_-_Blick_vom_gro%C3%9Fen_Triumphbogen.jpg',
        id: 'aadsfhbkhlk1241',
        title: 'Meetup in Paris',
        date: new Date(),
        location: 'Paris',
        description: 'It\'s Paris!'
      }
    ],
    user: null,
    loading: false,
    error: null
  },






  mutations: {
    setLoadedMeetups (state, payload) {
      state.loadedMeetups = payload
    },
    createMeetup (state, payload) {
      state.loadedMeetups.push(payload)
    },
    setUser (state, payload) {
      state.user = payload
      this.commit('clearError')
      this.commit('setLoading', false)
      this.dispatch('checkIfUserEmailIsVerified', payload)
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    clearError (state) {
      state.error = null
    },
    onError (state, error) {
      this.commit('setLoading', false)
      this.commit('setError', {color:'red lighten-2', text:error})
      console.log(error)
    }
  },







  actions: {
    loadMeetups ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('meetups').once('value')
        .then((data) => {
          const meetups = []
          const obj = data.val()
          for (let key in obj) {
            meetups.push({
              id: key,
              title: obj[key].title,
              description: obj[key].description,
              imageUrl: obj[key].imageUrl,
              date: obj[key].date,
              creatorId: obj[key].creatorId
            })
          }
          commit('setLoadedMeetups', meetups)
          commit('setLoading', false)
        })
        .catch(
          (error) => {
            console.log(error)
            commit('setLoading', false)
          }
        )
    },
    createMeetup ({commit, getters}, payload) {
      const meetup = {
        title: payload.title,
        location: payload.location,
        imageUrl: payload.imageUrl,
        description: payload.description,
        date: payload.date.toISOString(),
        creatorId: getters.user.id
      }
      firebase.database().ref('meetups').push(meetup)
        .then((data) => {
          const key = data.key
          commit('createMeetup', {
            ...meetup,
            id: key
          })
        })
        .catch((error) => {
          console.log(error)
        })
      // Reach out to firebase and store it
    },




    signUserUp ({commit, dispatch}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then( user => {
          commit('setUser', user)

          firebase.auth().currentUser.sendEmailVerification()
            .then( () => console.log(`Email enviado`))
            .catch( err => commit('onError', err))

          firebase.auth().currentUser.getIdToken()
            .then(token => {
              // Send http api to create profile and update display name
              Vue.axios.post(`https://us-central1-ytube-vuejs-firestore-tutorial.cloudfunctions.net/httpApi/profileupdate`,
                {
                  uid: firebase.auth().currentUser.uid,
                  name: payload.name,
                  lastname: payload.lastname
                },
                {
                  headers: {'Authorization': `Bearer ${token}`}
                })
                .then(response => {
                  commit('setUser', response)

                }).catch( err => commit('onError', err) )
            }).catch( err => commit('onError', err) )
        }).catch( err => commit('onError', err.message) )
    },

    //   // {
    //   //   email: payload.email,
    //   //   password: payload.password,
    //   //   emailVerified: false,
    //   //   phoneNumber: "+11234567890",
    //   //   displayName: (payload.name + ' ' + payload.lastname),
    //   //   photoURL: "http://www.example.com/12345678/photo.png",
    //   //   disabled: false
    //   // }



    signUserIn ({commit, dispatch}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then( user => dispatch('getUser', user.uid))
        .catch( err => commit('onError', err.message))
    },




    getUser ({commit}, uid) {
      //db().collection('users').where('fb_id', '==', uid).limit(1).get()
      //commit('setUser', qS.docs[0].data())
      db().collection('users').doc(uid).get()
      .then( doc => {
        commit('setUser', doc.data())
      })
      .catch( err => {
        commit('onError', err)
      })
    },

    checkIfUserEmailIsVerified ({commit}, user) {
      if (user && !user.emailVerified) {
        commit('setError', {color:'yellow lighten-2', text:'Seu email ainda não foi verificado', hasClick: {action: 'resendVerificationEmail', text: 'Reenviar e-mail'}})
      }
    },

    resendVerificationEmail ({commit}, payload) {
      firebase.auth().currentUser.sendEmailVerification()
        .then( () => console.log(`Email enviado`))
        .catch( err => commit('onError', err))
    },

    autoSignIn ({commit}, user) {
      commit('setUser', user)
    },


    logout ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    },


    clearError ({commit}) {
      commit('clearError')
    }
  },










  getters: {
    loadedMeetups (state) {
      return state.loadedMeetups.sort((meetupA, meetupB) => {
        return meetupA.date > meetupB.date
      })
    },
    featuredMeetups (state, getters) {
      return getters.loadedMeetups.slice(0, 5)
    },
    loadedMeetup (state) {
      return (meetupId) => {
        return state.loadedMeetups.find((meetup) => {
          return meetup.id === meetupId
        })
      }
    },
    user (state) {
      return state.user
    },
    loading (state) {
      return state.loading
    },
    error (state) {
      return state.error
    }
  }
})
