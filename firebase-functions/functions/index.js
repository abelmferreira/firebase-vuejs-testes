'use strict';

const functions = require('firebase-functions')

// Auth Events
const evtAuth = require('./src/eventsAuth.js')
exports.profileCreate = functions.auth.user().onCreate(evtAuth.onCreate)
exports.profileSetDeleted = functions.auth.user().onDelete(evtAuth.onDelete)

// Http Events
const app = require('./config/express')
require('./src/eventsHttpUsers.js')(app)
exports.httpApi = functions.https.onRequest(app)

