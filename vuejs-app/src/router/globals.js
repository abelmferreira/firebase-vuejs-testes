import AuthGuard from './auth-guard'

import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'


export default [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/hello',
      name: 'hello',
      component: HelloWorld,
      beforeRouteEnter: AuthGuard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      redirect: '/'
    },
  ]