const admin = require('./firebase.js')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')({origin: true})

var app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// cors
app.use(cors)

// log middleware
app.use(function(req, res, next) {
  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  console.log(`Resquest ${req.method} to ${req.url}`)
  next();
})

// auth middleware
app.use((req, res, next) => {
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
    res.status(403).send('Unauthorized1');
    return;
  }
  const idToken = req.headers.authorization.split('Bearer ')[1];
  admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
    req.user = decodedIdToken;
    //console.log(decodedIdToken)
    next()
  }).catch(error => {
    res.status(403).send(`Unauthorized`);
  })
})

module.exports = app





