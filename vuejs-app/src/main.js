import Vue from 'vue'
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
// Vue.use(ElementUI)

import App from './App'
import router from './router'
import { store } from './store'

import firebase from '@/config/firebase'
import AlertCmp from '@/components/Shared/Alert.vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)
Vue.component('show-alert', AlertCmp)

Vue.config.productionTip = false

// Técnica caso a aplicação redirecione incorretamente para a tela de login no primeiro load
// firebase possui um observer que dispara somente quando o auth estiver carregado
// let app
// firebase.auth().onAuthStateChanged(user => {
//   if (!app) {
//     app = new Vue.({})
//   }
// })
//
// Modelo tradicional apenas chama new Vue()... e o monitor onAuthStateChanged fica dentro do created
//


let app
firebase.auth().onAuthStateChanged(user => {
  if (user) {
    store.dispatch('autoSignIn', user)
    //firebase.auth().currentUser.getIdToken().then(token => console.log(token))
  }

  if (!app) {
    app = new Vue({
      el: '#app',
      router,
      store,
      components: { App },
      template: '<App/>',
      created () {
        // this.$store.dispatch('loadMeetups')
      }
    })
  }
})