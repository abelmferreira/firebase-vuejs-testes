import AuthGuard from './auth-guard'

import Signin from '@/components/User/Signin'
import Signup from '@/components/User/Signup'
import Profile from '@/components/User/Profile'

export default [
    {
      path: '/user/signin',
      name: 'signin',
      component: Signin
    },
    {
      path: '/user/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/user/profile',
      name: 'user-profile',
      component: Profile,
      beforeEnter: AuthGuard,
      meta: {
        requiresAuth: true
      }
    }
  ]
