const functions = require('firebase-functions')
const admin = require('firebase-admin')

const firebaseConf = {
  path : '../service-account.json',
  databaseUrl : "https://ytube-vuejs-firestore-tutorial.firebaseio.com"
}

module.exports = admin.initializeApp({
    credential: admin.credential.cert(require(firebaseConf.path)),
    databaseURL: firebaseConf.databaseUrl
  })

//admin.initializeApp(functions.config().firebase)


