const admin = require('../config/firebase.js')
const FieldValue = require('firebase-admin').firestore.FieldValue

const onCreateAuthEvent = event => {
  const user = event.data
  return admin.firestore().collection('users').doc(user.uid).set({
    user_id: user.uid,
    email: user.email,
    deleted: false,
    created_at: FieldValue.serverTimestamp()
  })
}

const onDeleteAuthEvent = event => {
  const uid = event.data.uid
  return admin.firestore().collection('users').doc(uid).update({
    deleted: true,
    deleted_at: FieldValue.serverTimestamp()
  })
}

module.exports = {
  'onCreate': onCreateAuthEvent,
  'onDelete': onDeleteAuthEvent
}