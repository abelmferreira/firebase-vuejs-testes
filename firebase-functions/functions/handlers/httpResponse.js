const handleError = (res, step, error, status = false) => {
  if (!status) status = 500
  console.error(`Error at ${step}: ${error}`)
  return res.status(status).json(error)
}

const handleSuccess = (res, body, status = false) => {
  if (!status) status = 200
  console.log(`Success response ${status} -> ${(JSON.stringify(body)).substr(0, 100)}`)
  return res.status(status).json(body)
}

module.exports = {
  'error': handleError,
  'success': handleSuccess
}
